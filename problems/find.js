//this cb find function will find a particular element in array

function cb(value, search) {
    return value === search;
}

function find1(elements, cb, search) {

    if (Array.isArray(elements)) {
        let bool = false;

        for (let i = 0; i < elements.length; i++) {
            bool = cb(elements[i], search);

            if (bool === true) {
                return (elements[i]);
            }
        }
        if (bool === false)
            return 'undfined';
    } else {
        console.log("passed parameter is not an array");
    }
}

module.exports = {
    cb,
    find1
}