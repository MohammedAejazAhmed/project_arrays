// filter will return only even elements of array

function cb(value) {
    if (value % 2 == 0)
        return false;
    else
        return true;
}

function filter1(elements, cb) {
    let array = [];
    if (Array.isArray(elements)) {
        let bool = false;

        for (let i = 0; i < elements.length; i++) {
            bool = cb(elements[i]);

            if (bool === true) {
                array.push(elements[i]);
            }
        }
        return array;
    } else {
        console.log("passed parameter is not an array");
    }
}

module.exports = {
    cb,
    filter1
}