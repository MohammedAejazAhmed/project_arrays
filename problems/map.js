//In this map function we are trying to map square values of elements to the elements

function cb(value) {
    return value * value;
}

function map1(elements, cb) {

    let newElements = [];
    if (Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            newElements[i] = cb(elements[i]);
        }
        return newElements;
    } else {
        console.log("parameter passed is not an array");
    }
}

module.exports = {
    map1,
    cb
}
