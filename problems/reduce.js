//reduce function will return sum of squares of all the array elements

function cb(startValue, currValue, index) {
    if (index === 0) {
        return currValue * currValue
    } else {
        return startValue + currValue * currValue
    }
}

function reduce(elements, cb, startingValue) {
    if (Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            startingValue = cb(startingValue, elements[i], i);
        }
        return startingValue;
    } else {
        console.log("Cannot reduce as 'elements' is not an array");
    }
}

module.exports = {
    cb,
    reduce
}